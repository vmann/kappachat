#include "login_dialog.hh"

#include <QDialogButtonBox>
#include <QFormLayout>
#include <QLabel>
#include <QMessageBox>
#include <QVBoxLayout>

namespace kappachat {

LoginDialog::LoginDialog(QWidget *parent, DeltachatContext *context)
    : QDialog{parent},
      m_context{context},
      m_email{new QLineEdit},
      m_password{new QLineEdit},
      m_imapuser{new QLineEdit},
      m_imapserver{new QLineEdit},
      m_smtpuser{new QLineEdit},
      m_smtpserver{new QLineEdit} {
  connect(m_context, &DeltachatContext::configureProgress, this,
          &LoginDialog::configureProgress);

  auto form = new QFormLayout;
  form->addRow(new QLabel{"Email:"}, m_email);
  form->addRow(new QLabel{"Password:"}, m_password);
  m_password->setEchoMode(QLineEdit::Password);
  form->addRow(new QLabel{"IMAP-user:"}, m_imapuser);
  form->addRow(new QLabel{"IMAP-server:"}, m_imapserver);
  form->addRow(new QLabel{"SMTP-user:"}, m_smtpuser);
  form->addRow(new QLabel{"SMTP-server:"}, m_smtpserver);

  auto buttons =
      new QDialogButtonBox{QDialogButtonBox::Ok | QDialogButtonBox::Cancel};
  connect(buttons, &QDialogButtonBox::accepted, this, &QDialog::accept);
  connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);

  auto vbox = new QVBoxLayout{this};
  vbox->addLayout(form);
  vbox->addWidget(buttons);

  setWindowTitle("Login");
  setModal(true);

  if (!m_context->isConfigured()) {
    show();
  }
}

void LoginDialog::accept() {
  m_context->setConfig("addr", m_email->text());
  m_context->setConfig("mail_pw", m_password->text());
  m_context->setConfig("mail_user", m_imapuser->text());
  m_context->setConfig("mail_server", m_imapserver->text());
  m_context->setConfig("send_user", m_smtpuser->text());
  m_context->setConfig("send_server", m_smtpserver->text());
  m_context->configure();
  m_progress = new QProgressDialog{"Connecting ...", "Cancel", 0, 1000,
                                   static_cast<QWidget *>(parent())};
  m_progress->setModal(true);
  connect(m_progress, &QProgressDialog::canceled, this,
          &LoginDialog::configureCanceled);
  QDialog::accept();
}

void LoginDialog::configureProgress(int permille, QString message) {
  if (permille && m_progress) {
    m_progress->setValue(permille);
    m_progress->setLabelText(message);
  } else {
    if (m_progress) {
      m_progress->close();
    }
    configureCanceled();

    QMessageBox messagebox;
    messagebox.setText("Login failed.");
    if (message.length() > 500) {
      messagebox.setDetailedText(message);
    } else {
      messagebox.setInformativeText(message);
    }
    messagebox.setIcon(QMessageBox::Critical);
    messagebox.exec();
  }
}

void LoginDialog::configureCanceled() {
  if (m_progress) {
    m_progress->deleteLater();
    m_progress = nullptr;
  }
  show();
}

}  // namespace kappachat
