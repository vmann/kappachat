#pragma once

#include "deltachat_context.hh"
#include "login_dialog.hh"
#include "main_window.hh"
#include <QApplication>

namespace kappachat {

class Kappachat : public QApplication {
  Q_OBJECT

 public:
  Kappachat(int argc, char **argv);

 private:
  DeltachatContext context;
  MainWindow main_window;
  LoginDialog login_dialog;
};

}  // namespace kappachat
