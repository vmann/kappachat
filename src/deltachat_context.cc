#include "deltachat_context.hh"

#include <QApplication>
#include <iostream>
#include <utility>

namespace kappachat {

DeltachatContext::DeltachatContext()
    : m_context{dc_context_new(NULL, "kappachat.sqlite", NULL)},
      m_event_thread{std::bind(&DeltachatContext::eventThread, this)} {
  if (isConfigured()) {
    dc_start_io(m_context);
  }
}

DeltachatContext::~DeltachatContext() {
  dc_stop_io(m_context);
  dc_context_unref(m_context);
  m_event_thread.join();
}

void DeltachatContext::setConfig(QString key, QString value) {
  dc_set_config(m_context, key.toStdString().c_str(),
                value.toStdString().c_str());
}

bool DeltachatContext::isConfigured() const {
  return dc_is_configured(m_context);
}

void DeltachatContext::configure() {
  dc_configure(m_context);
}

void DeltachatContext::eventThread() {
  dc_event_emitter_t *emitter = dc_get_event_emitter(m_context);
  while (dc_event_t *event = dc_get_next_event(emitter)) {
    switch (dc_event_get_id(event)) {
      case DC_EVENT_CHAT_EPHEMERAL_TIMER_MODIFIED: {
        int chat_id = dc_event_get_data1_int(event);
        int timer_value = dc_event_get_data2_int(event);
        emit chatEphemeralTimerModified(chat_id, timer_value);
        break;
      }
      case DC_EVENT_CHAT_MODIFIED: {
        int chat_id = dc_event_get_data1_int(event);
        emit chatModified(chat_id);
        break;
      }
      case DC_EVENT_CONFIGURE_PROGRESS: {
        int permille = dc_event_get_data1_int(event);
        char *message = dc_event_get_data2_str(event);
        emit configureProgress(permille, message ?: "");
        dc_str_unref(message);
        if (permille == 1000) {
          dc_start_io(m_context);
        }
        break;
      }
      case DC_EVENT_CONNECTIVITY_CHANGED: {
        emit connectivityChanged();
        break;
      }
      case DC_EVENT_CONTACTS_CHANGED: {
        int contact_id = dc_event_get_data1_int(event);
        emit contactsChanged(contact_id);
        break;
      }
      case DC_EVENT_DELETED_BLOB_FILE: {
        char *path = dc_event_get_data2_str(event);
        emit deletedBlobFile(path);
        dc_str_unref(path);
        break;
      }
      case DC_EVENT_ERROR: {
        char *message = dc_event_get_data2_str(event);
        std::clog << "Error: " << message << std::endl;
        emit error(message);
        dc_str_unref(message);
        break;
      }
      case DC_EVENT_ERROR_SELF_NOT_IN_GROUP: {
        char *message = dc_event_get_data2_str(event);
        std::clog << "Error: self not in group: " << message << std::endl;
        emit errorSelfNotInGroup(message);
        dc_str_unref(message);
        break;
      }
      case DC_EVENT_IMAP_CONNECTED: {
        char *message = dc_event_get_data2_str(event);
        std::clog << "Info: IMAP connected: " << message << std::endl;
        emit imapConnected(message);
        dc_str_unref(message);
        break;
      }
      case DC_EVENT_IMAP_MESSAGE_DELETED: {
        char *message = dc_event_get_data2_str(event);
        std::clog << "Info: IMAP message delted: " << message << std::endl;
        emit imapMessageDeleted(message);
        dc_str_unref(message);
        break;
      }
      case DC_EVENT_IMAP_MESSAGE_MOVED: {
        char *message = dc_event_get_data2_str(event);
        std::clog << "Info: IMAP message moved: " << message << std::endl;
        emit imapMessageMoved(message);
        dc_str_unref(message);
        break;
      }
      case DC_EVENT_IMEX_FILE_WRITTEN: {
        char *path = dc_event_get_data2_str(event);
        emit imexFileWritten(path);
        dc_str_unref(path);
        break;
      }
      case DC_EVENT_IMEX_PROGRESS: {
        int permille = dc_event_get_data1_int(event);
        emit locationChanged(permille);
        break;
      }
      case DC_EVENT_INCOMING_MSG: {
        int chat_id = dc_event_get_data1_int(event);
        int msg_id = dc_event_get_data2_int(event);
        emit incomingMsg(chat_id, msg_id);
        break;
      }
      case DC_EVENT_INFO: {
        char *message = dc_event_get_data2_str(event);
        std::clog << "Info: " << message << std::endl;
        emit info(message);
        dc_str_unref(message);
        break;
      }
      case DC_EVENT_LOCATION_CHANGED: {
        int contact_id = dc_event_get_data1_int(event);
        emit locationChanged(contact_id);
        break;
      }
      case DC_EVENT_MSG_DELIVERED: {
        int chat_id = dc_event_get_data1_int(event);
        int msg_id = dc_event_get_data2_int(event);
        emit msgDelivered(chat_id, msg_id);
        break;
      }
      case DC_EVENT_MSG_FAILED: {
        int chat_id = dc_event_get_data1_int(event);
        int msg_id = dc_event_get_data2_int(event);
        emit msgFailed(chat_id, msg_id);
        break;
      }
      case DC_EVENT_MSG_READ: {
        int chat_id = dc_event_get_data1_int(event);
        int msg_id = dc_event_get_data2_int(event);
        emit msgRead(chat_id, msg_id);
        break;
      }
      case DC_EVENT_MSGS_CHANGED: {
        int chat_id = dc_event_get_data1_int(event);
        int msg_id = dc_event_get_data2_int(event);
        emit msgsChanged(chat_id, msg_id);
        break;
      }
      case DC_EVENT_MSGS_NOTICED: {
        int chat_id = dc_event_get_data1_int(event);
        emit msgsNoticed(chat_id);
        break;
      }
      case DC_EVENT_NEW_BLOB_FILE: {
        char *path = dc_event_get_data2_str(event);
        emit newBlobFile(path);
        dc_str_unref(path);
        break;
      }
      case DC_EVENT_REACTIONS_CHANGED: {
        int chat_id = dc_event_get_data1_int(event);
        int msg_id = dc_event_get_data2_int(event);
        emit reactionsChanged(chat_id, msg_id);
        break;
      }
      case DC_EVENT_SECUREJOIN_INVITER_PROGRESS: {
        int contact_id = dc_event_get_data1_int(event);
        int permille = dc_event_get_data2_int(event);
        emit securejoinInviterProgress(contact_id, permille);
        break;
      }
      case DC_EVENT_SECUREJOIN_JOINER_PROGRESS: {
        int contact_id = dc_event_get_data1_int(event);
        int permille = dc_event_get_data2_int(event);
        emit securejoinJoinerProgress(contact_id, permille);
        break;
      }
      case DC_EVENT_SELFAVATAR_CHANGED: {
        emit selfavatarChanged();
        break;
      }
      case DC_EVENT_SMTP_CONNECTED: {
        char *message = dc_event_get_data2_str(event);
        emit smtpConnected(message);
        dc_str_unref(message);
        break;
      }
      case DC_EVENT_SMTP_MESSAGE_SENT: {
        char *message = dc_event_get_data2_str(event);
        emit smtpMessageSent(message);
        dc_str_unref(message);
        break;
      }
      case DC_EVENT_WARNING: {
        char *message = dc_event_get_data2_str(event);
        std::clog << "Warning: " << message << std::endl;
        emit warning(message);
        dc_str_unref(message);
        break;
      }
      case DC_EVENT_WEBXDC_INSTANCE_DELETED: {
        int msg_id = dc_event_get_data1_int(event);
        emit webXDCInstanceDeleted(msg_id);
        break;
      }
      case DC_EVENT_WEBXDC_STATUS_UPDATE: {
        int msg_id = dc_event_get_data1_int(event);
        int status_update_serial = dc_event_get_data2_int(event);
        emit webXDCStatusUpdate(msg_id, status_update_serial);
        break;
      }
    }
    dc_event_unref(event);
  }
  dc_event_emitter_unref(emitter);
}

}  // namespace kappachat
