#pragma once

#include <deltachat.h>

#include <QObject>
#include <QString>
#include <thread>

namespace kappachat {

class DeltachatContext : public QObject {
  Q_OBJECT

 public:
  DeltachatContext();
  ~DeltachatContext();

  bool isConfigured() const;

 public slots:
  void setConfig(QString key, QString value);
  void configure();

 signals:
  void chatEphemeralTimerModified(int chat_id, int timer_value);
  void chatModified(int chat_id);
  void configureProgress(int permille, QString message);
  void connectivityChanged();
  void contactsChanged(int contact_id);
  void deletedBlobFile(QString path);
  void error(QString message);
  void errorSelfNotInGroup(QString message);
  void imapConnected(QString message);
  void imapMessageDeleted(QString message);
  void imapMessageMoved(QString message);
  void imexFileWritten(QString path);
  void imexProgress(int permille);
  void incomingMsg(int chat_id, int msg_id);
  void info(QString message);
  void locationChanged(int contact_id);
  void msgDelivered(int chat_id, int msg_id);
  void msgFailed(int chat_id, int msg_id);
  void msgRead(int chat_id, int msg_id);
  void msgsChanged(int chat_id, int msg_id);
  void msgsNoticed(int chat_id);
  void newBlobFile(QString path);
  void reactionsChanged(int chat_id, int msg_id);
  void securejoinInviterProgress(int contact_id, int permille);
  void securejoinJoinerProgress(int contact_id, int permille);
  void selfavatarChanged();
  void smtpConnected(QString message);
  void smtpMessageSent(QString message);
  void warning(QString message);
  void webXDCInstanceDeleted(int msg_id);
  void webXDCStatusUpdate(int msg_id, int status_update_serial);

 private:
  void eventThread();

  dc_context_t *m_context;
  std::thread m_event_thread;
};

}  // namespace kappachat
