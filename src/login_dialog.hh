#pragma once

#include "deltachat_context.hh"
#include <QDialog>
#include <QLineEdit>
#include <QProgressDialog>

namespace kappachat {

class LoginDialog : public QDialog {
  Q_OBJECT

 public:
  LoginDialog(QWidget *parent, DeltachatContext *context);

 public slots:
  void accept() override;
  void configureProgress(int permille, QString message);
  void configureCanceled();

 private:
  DeltachatContext *m_context;
  QLineEdit *m_email;
  QLineEdit *m_password;
  QLineEdit *m_imapuser;
  QLineEdit *m_imapserver;
  QLineEdit *m_smtpuser;
  QLineEdit *m_smtpserver;
  QProgressDialog *m_progress;
};

}  // namespace kappachat
