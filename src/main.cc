#include "main.hh"

int main(int argc, char **argv) {
  kappachat::Kappachat app{argc, argv};
  return app.exec();
}

namespace kappachat {

Kappachat::Kappachat(int argc, char **argv)
    : QApplication{argc, argv}, login_dialog{&main_window, &context} {
  connect(&login_dialog, &LoginDialog::rejected, &main_window,
          &MainWindow::close);
}

}  // namespace kappachat
